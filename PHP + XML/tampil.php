<!-- Tugas Web Semantik 
	XML <=> PHP   

	-->

<!--Untuk menjalankan program tanpa internet 
	terlebih dahulu simpan file film_movie.xml 
	dan .php nya serta gambar" yang berhubungan
	kedalam folder	htdocs di xampp ,
	lalu aktifkan apache,
	nah, file php ini dapat langsung dijalankan
	di web browser
	-->
	
<!DOCTYPE html> <!--Dokumentasi HTML 5-->
<html> <!--Penanda jenis dokumen HTML-->
	<head> <!--Informasi Mengenai Laman Web-->
	<!--Judul Laman-->	
		<title>Daftar Film</title>
	<!--Bagian CSS untuk Styling Tabel
		Website dilengkapi gambar2 yang juga harus di download dulu-->
		<style type="text/css">
			body{
				background: #fff url(indonesia.jpg) no-repeat fixed center;
				width:100%;
				height:100%;
				 
				}
			.judul{
				display: flex;
				-webkit-box-align: center;
				align-items: center;
				width: 100%;
				margin: 4% 0%;
				-webkit-box-pack: center;
				justify-content: center;
				-webkit-tap-highlight-color: rgba(0,0,0,0);
				box-sizing: border-box;
			}			
			.logokiri{
				position: relative;
				box-shadow: 0px 0px 40px #868686;
				width: 937px;
				height: 572px;
				background: rgba(255, 255, 255, 0) url(../images/box-background.png) no-repeat fixed center;
			}
			.aksen{
				min-width: 50%;
				position: absolute;
				top: -20%;
				left: -20%;
				width: 30%;
				
			}
			
			.sponsor{
				position: absolute;
				top: 10px;
				right: 10px;
				width: 150px;
			}
			h2{
				text-align:center;
				padding-top: 0%;
				
			}
			
				
			table{
				box-shadow: 0px 0px 40px #868686;
				width: 900px;
				height: 500px;
				background: rgba(255, 255, 255, 0) url(box-background.png) no-repeat fixed center;
				display: inline-block;
				border-radius: 4px;
				box-sizing: border-box;
				box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
				background-position: center;
				background-repeat: no-repeat;
				padding: 33px 10px 50px 100px;
				margin-right: -15px;
				margin-left: -15px;
				margin-top:40px;

				margin-left:10%;
			}
			th,td {
				border : 0px ;
				border-collapse: collapse;
				padding: 5px;
			}
			th{
				background-color:red;
				color: white;
				
				 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); 
			}
			
			table th:hover{
					color: gold;
					}
			table tr:hover{
				width:100%;
				box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		  
			}
		</style>
	</head>
	
	<body>
		<div class="judul">
			<div class="logokiri">
				<div>
					<img src="aksen.png" class="aksen">
					<h2 > <font face="Broadway"> Home Of Movie </font> </h2>
				</div>
				<img src="hbo.png" class="sponsor">
				
				<!--tabel yang berisi data xml yang telah diresponse server-->
			
		<?php
			
		//untuk meload data xml (film.xml) dengan cara SimpleXML	
			$mymovie = simplexml_load_file('film_movie.xml');  
			
		//membuat tabel	
			echo"<table width=100% cellpadding=10 cellspacing=0>"; 
			echo"<tr>";
			echo"<th> Judul Film</th>";
			echo"<th> Genre </th>";
			echo"<th> Tahun </th>";
			echo"<th> Sutradara </th>";
			echo"<th> Pemain </th>";
			echo"</tr>";
		
		// menggunakan looping untuk menampilkan setiap data dari XML ke dalam tabel HTML		
			for($awal=0; $awal < sizeof($mymovie); $awal++){ 
				echo "<tr>";
				echo "<td>".$mymovie->film[$awal]-> judul."</td>";
				echo "<td>".$mymovie->film[$awal]-> kategori."</td>";
				echo "<td>".$mymovie->film[$awal]-> tahun."</td>";
				echo "<td>".$mymovie->film[$awal]-> sutradara."</td>";
				echo "<td>".$mymovie->film[$awal]-> cast."</td>";
				echo "</tr>";
			}
			echo "</table>";
		?>
		</div>
		</div>
		
	<br> <br>
		<center> Copyright&copy 2017 -Kelompok4-</center>
	</body>
</html>